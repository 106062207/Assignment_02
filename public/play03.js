var play03State = {
  create: function(){
    //map
    this.map = game.add.tileSprite(0, 0, 450, 550, 'background');
    this.backgroundSound = game.add.audio('BGM01');
    this.backgroundSound.loop = true;
    this.backgroundSound.volume = 0.3;
    this.backgroundSound.play();

    //player
    this.player = game.add.sprite(game.width/2, game.height/2, 'player');
    game.physics.arcade.enable(this.player);
    this.player.anchor.setTo(0.5, 1);
    this.player.animations.add('frontwalk', [0, 2], 8, true);
    this.player.animations.add('leftwalk', [3, 5], 8, true);
    this.player.animations.add('rightwalk', [6, 8], 8, true);
    this.player.animations.add('backwalk', [9, 11], 8, true);
    this.playerHP = 0;
    this.playerDieSound = game.add.audio('playerDie');
    this.playerUltSound = game.add.audio('ult');
    this.helperCount = 0;

    //playerMove
    this.dir = 0;

    //input
    this.cursor = game.input.keyboard.createCursorKeys();
    this.zKey = game.input.keyboard.addKey(Phaser.Keyboard.Z);
    this.xKey = game.input.keyboard.addKey(Phaser.Keyboard.X);
    this.escKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
    this.shiftKey = game.input.keyboard.addKey(Phaser.Keyboard.SHIFT);
    this.wKey = game.input.keyboard.addKey(Phaser.Keyboard.W);
    this.sKey = game.input.keyboard.addKey(Phaser.Keyboard.S);

    //bullet
    this.bullets = game.add.group();
    this.bullets.enableBody = true;
    this.bullets.createMultiple(20, 'bullet');
    this.bulletCount = 0;
    this.bulletAngle = [Math.PI*(1/8), Math.PI*(1/4), Math.PI*(3/8), Math.PI*(1/2), Math.PI*(5/8), Math.PI*(3/4), Math.PI*(7/8)];

    //enemy: knight
    this.knight = game.add.sprite(game.width/2, game.height-120, 'knight01');
    game.physics.arcade.enable(this.knight);
    this.knight.anchor.setTo(0.5, 0);
    this.knight.animations.add('frontwalk', [0, 2], 8, true);
    this.knight.animations.add('leftwalk', [3, 5], 8, true);
    this.knight.animations.add('rightwalk', [6, 8], 8, true);
    this.knight.animations.add('backwalk', [9, 11], 8, true);
    this.knight.body.velocity.x = 50;
    this.knight.body.velocity.y = 50;
    this.bulletCountKnight = 0;
    this.knightHP = 0;
    this.knightBullets = game.add.group();
    this.knightBullets.enableBody = true;
    this.knightBullets.createMultiple(10, 'knightBullet');
    this.knightBullets.callAll('animations.add', 'animations', 'frontwalk', [0, 1], 8, true);
    this.knightBullets.callAll('animations.add', 'animations', 'backwalk', [2, 3], 8, true);
    this.speedUp = false;

    //item
    this.items = game.add.group();
    this.items.enableBody = true;
    this.items.createMultiple(5, 'item');
    this.itemCount = 600;

    //helper
    this.helpers = game.add.group();
    this.helpers.enableBody = true;
    this.helpers.createMultiple(1, 'helper');
    this.helpers.callAll('animations.add', 'animations', 'frontwalk', [0, 2], 8, true);
    this.helpers.callAll('animations.add', 'animations', 'leftwalk', [3, 5], 8, true);
    this.helpers.callAll('animations.add', 'animations', 'rightwalk', [6, 8], 8, true);
    this.helpers.callAll('animations.add', 'animations', 'backwalk', [9, 11], 8, true);
    this.helperBullets = game.add.group();
    this.helperBullets.enableBody = true;
    this.helperBullets.createMultiple(20, 'bullet');
    this.bulletCountHelper = 0;

    //playerHealth
    this.healthLabel = game.add.text(5, 5, 'Health', {font:'18px Arial', fill:'#ffffff'});
    this.healthBar = game.add.graphics(0, 0);
    this.playerHealth();

    //mana
    this.manaLabel = game.add.text(5, 45, 'Mana', {font:'18px Arial', fill:'#ffffff'});
    this.manaBar = game.add.graphics(0, 0);
    this.mana = 5;
    this.playerMana();
    this.manaSound = game.add.audio('mana');
    this.manaReady = false;

    //score
    this.scoreLabel = game.add.text(5, 85, 'score: 0', {font:'18px Arial', fill:'#ffffff'});
    game.global.score = 0;

    //volume
    this.volumeLabel = game.add.text(game.width-120, 5, 'BGM', {font:'18px Arial', fill:'#ffffff'});
    this.volumeBar = game.add.graphics(0, 0);
    this.volumeControl();
    this.wKey.onDown.add(function(){
      if((this.backgroundSound.volume+0.1) >= 1){
        this.backgroundSound.volume = 1;
      }
      else{
        this.backgroundSound.volume += 0.1;
      }
    }, this);
    this.sKey.onDown.add(function(){
      if((this.backgroundSound.volume-0.1) <= 0){
        this.backgroundSound.volume = 0;
      }
      else{
        this.backgroundSound.volume -= 0.1;
      }
    }, this);

    //pause
    this.escKey.onDown.add(this.Pause, this);
    this.pauseLabel = game.add.text(game.width-40, 35, 'Pause', {font:'18px, Arial', fill:'#ffffff'});
    this.pauseLabel.visible = false;

    //particle
    this.emitter = game.add.emitter(0, 0, 30);
    this.emitter.makeParticles('pixel');
    this.emitter.setYSpeed(-150, 150);
    this.emitter.setXSpeed(-150, 150);
    this.emitter.setScale(1.5, 0, 1.5, 0, 800);
    this.emitter.gravity = 0;
  },

  update: function(){
    if(this.player.alive){
      this.map.tilePosition.y -= 2;
      game.physics.arcade.overlap(this.player, this.knightBullets, function(player, bullet){
        this.playerHurt(bullet);
      }, null, this);

      game.physics.arcade.overlap(this.knight, this.bullets, function(knight, bullet){
        this.enemyHurt(knight, bullet);
      }, null, this);

      game.physics.arcade.overlap(this.knight, this.helperBullets, function(knight, bullet){
        this.enemyHurt(knight, bullet);
      }, null, this);

      game.physics.arcade.overlap(this.player, this.items, function(player, item){
        this.helperGen();
        item.kill();
        game.global.score += 5;
        this.scoreLabel.text = 'score: ' + game.global.score;
      }, null, this);
    }

    this.playerMove();

    this.enemyMove();

    this.bulletMove();

    this.itemGen();

    this.helperMove();

    if(!this.player.inWorld){
      this.playerDie();
    }

    this.volumeControl();
  },


  //player
  playerMove: function(){
    var speed;
    if(this.shiftKey.isDown){
      speed = 50;
    }
    else{
      speed = 200;
    }
    if(this.cursor.left.isDown){
      this.dir = 1;
      this.player.body.velocity.x = speed * (-1);
      this.player.animations.play('leftwalk');
    }
    else if(this.cursor.right.isDown){
      this.dir = 2;
      this.player.body.velocity.x = speed;
      this.player.animations.play('rightwalk');
    }
    else{
      this.player.body.velocity.x = 0;
      if(this.dir == 1){
        this.player.frame = 3;
      }
      else if(this.dir == 2){
        this.player.frame = 6;
      }
    }

    if(this.cursor.up.isDown){
      this.dir = 4;
      this.player.body.velocity.y = speed * (-1);
      this.player.animations.play('backwalk');
    }
    else if(this.cursor.down.isDown){
      this.dir = 0;
      this.player.body.velocity.y = speed;
      this.player.animations.play('frontwalk');
    }
    else{
      this.player.body.velocity.y = 0;
      if(this.dir == 0){
        this.player.frame = 0;
      }
      else if(this.dir == 4){
        this.player.frame = 9;
      }
    }

    if(this.player.x <= 16){
      this.player.x = 16;
    }
    else if(this.player.x >= (game.width-16)){
      this.player.x = (game.width-16);
    }

    if(this.player.y <= 32){
      this.player.y = 32;
    }
    else if(this.player.y >= (game.height-32)){
      this.player.y = (game.height-32);
    }

    if(this.zKey.isDown){
      if(this.bulletCount == 10){
        this.bulletCount = 0;
        this.attack(this.player.x, this.player.y, 200, 'normal');
      }
      else{
        this.bulletCount++;
      }
    }

    if(this.xKey.isDown && this.mana == 100){
      this.playerUlt();
      this.manaReady = false;
      this.mana = 0;
    }
  },

  attack: function(x, y, s, type){
    var bullet = this.bullets.getFirstDead();
    if(!bullet){return;}
    bullet.anchor.setTo(0.5, 0.5);
    bullet.scale.setTo(0.25, 0.25);
    bullet.reset(x, y);
    bullet.checkWorldBounds = true;
    bullet.outOfBoundsKill = true;
    bullet.body.velocity.x = 0;
    bullet.body.velocity.y = s;
  },

  playerUlt: function(){
    this.playerUltSound.play();
    this.knightHP += 100;
    this.bullets.forEachAlive(function(child){
      child.kill();
    }, this);
    this.knightBullets.forEachAlive(function(child){
      child.kill();
    })
  },

  playerHurt: function(bullet){
    //console.log(this.playerHP);
    this.emitter.x = this.player.x;
    this.emitter.y = this.player.y;
    this.emitter.start(true, 300, null, 5);
    
    bullet.kill();
    this.playerHealth();
    if(this.playerHP >= 10){
      this.playerHP = 10;
      this.playerDie();
    }
    else{
      this.playerHP += (10/3);
    }
  },

  playerDie: function(){
    this.playerDieSound.play();
    this.player.kill();

    this.emitter.x = this.player.x;
    this.emitter.y = this.player.y;
    this.emitter.start(true, 800, null, 15);

    game.camera.shake(0.02, 300);

    this.backgroundSound.fadeOut(1000);

    game.time.events.add(2500, function(){
      this.backgroundSound.stop();
      game.state.start('over');
    }, this);
  },

  itemGen: function(){
    if(this.itemCount == 1200){
      var item = this.items.getFirstDead();
      if(!item){return;}
      item.anchor.setTo(0.5, 0.5);
      item.scale.setTo(0.5, 0.5);
      var itemX = game.rnd.pick([1, 1.5, 2, 2.5, 3, 3.5, 4]) * 100;
      var itemY = game.rnd.pick([1, 1.5, 2, 2.5, 3]) * 100;
      item.reset(itemX, itemY);
      game.add.tween(item.scale).to({x: 0.5, y: 0.5}, 300).start();

      this.itemCount = 0;
    }
    else{
      this.itemCount++;
    }
  },

  helperGen: function(){
    var helper = this.helpers.getFirstDead();
    if(!helper){return;}
    helper.reset(game.width/2, game.height/2);
    helper.anchor.setTo(0.5, 1);
    var dirX = game.rnd.pick([-1, 1]);
    var dirY = game.rnd.pick([-1, 0, 1]);
    helper.body.velocity.x = 100 * dirX;
    helper.body.velocity.y = 100 * dirY;
  },

  helperMove: function(){
    var helper = this.helpers.getFirstAlive();
    if(!helper){return;}

    if(this.helperCount == 600){
      helper.kill();
      this.helperCount = 0;
      this.bulletCountHelper = 0;
      return;
    }
    else{
      this.helperCount++;
    }

    if(helper.body.velocity.y < 0){
      helper.animations.play('backwalk');
      if(helper.y <= 50){
        helper.body.velocity.y *= -1;
      }

      if(helper.body.velocity.x < 0 && helper.x <= 21){
        helper.body.velocity.x *= -1;
      }
      if(helper.body.velocity.x > 0 && helper.x >= (game.width-21)){
        helper.body.velocity.x *= -1;
      }
    }
    else if(helper.body.velocity.y > 0){
      helper.animations.play('frontwalk');
      if(helper.y >= (game.height/2)){
        helper.body.velocity.y *= -1;
      }

      if(helper.body.velocity.x < 0 && helper.x <= 21){
        helper.body.velocity.x *= -1;
      }
      if(helper.body.velocity.x > 0 && helper.x >= (game.width-21)){
        helper.body.velocity.x *= -1;
      }
    }
    else{
      if(helper.body.velocity.x < 0){
        helper.animations.play('leftwalk');
        if(helper.x <= 21){
          helper.body.velocity.x *= -1;
        }
      }
      else if(helper.body.velocity.x > 0){
        helper.animations.play('rightwalk');
        if(helper.x >= (game.width-21)){
          helper.body.velocity.x *= -1;
        }
      }
    }

    if(this.bulletCountHelper == 200){
      this.helperAttack(helper.x, helper.y);
      this.bulletCountHelper = 0;
    }
    else{
      this.bulletCountHelper++;
    }
  },

  helperAttack: function(x, y){
    for(var i = 0; i < 7; i++){
      var bullet = this.helperBullets.getFirstDead();
      if(!bullet){return;}
      bullet.anchor.setTo(0.5, 0.5);
      bullet.scale.setTo(0.25, 0.25);
      bullet.reset(x, y);
      bullet.checkWorldBounds = true;
      bullet.outOfBoundsKill = true;
      bullet.body.velocity.x = 70 * Math.cos(this.bulletAngle[i]);
      bullet.body.velocity.y = 70 * Math.sin(this.bulletAngle[i]);
    }
  },

  //enemy
  enemyMove: function(){
    if(this.bulletCountKnight == 300){
      this.enemyAttack(this.knight.x, this.knight.y, -100, 'knight');
      this.bulletCountKnight = 0;
    }
    else{
      this.bulletCountKnight++;
    }

    if(this.knightHP >= 400 && !this.speedUp){
      console.log('speedup');
      game.camera.flash(0xffffff, 300);
      this.knight.body.velocity.x *= 5;
      this.knight.body.velocity.y *= 5;
      this.speedUp = true;
    }

    if(this.knight.body.velocity.y < 0){
      this.knight.animations.play('backwalk');
      if(this.knight.y <= (game.height/2)){
        this.knight.body.velocity.y *= -1;
      }

      if(this.knight.body.velocity.x < 0 && this.knight.x <= 30){
        this.knight.body.velocity.x *= -1;
      }
      if(this.knight.body.velocity.x > 0 && this.knight.x >= (game.width-30)){
        this.knight.body.velocity.x *= -1;
      }
    }
    else if(this.knight.body.velocity.y > 0){
      this.knight.animations.play('frontwalk');
      if(this.knight.y >= (game.height - 60)){
        this.knight.body.velocity.y *= -1;
      }

      if(this.knight.body.velocity.x < 0 && this.knight.x <= 30){
        this.knight.body.velocity.x *= -1;
      }
      if(this.knight.body.velocity.x > 0 && this.knight.x >= (game.width-30)){
        this.knight.body.velocity.x *= -1;
      }
    }
    else{
      if(this.knight.body.velocity.x < 0){
        this.knight.animations.play('leftwalk');
        if(this.knight.x <= 30){
          this.knight.body.velocity.x *= -1;
        }
      }
      else if(this.knight.body.velocity.x > 0){
        this.knight.animations.play('rightwalk');
        if(this.knight.x >= (game.width-30)){
          this.knight.body.velocity.x *= -1;
        }
      }
    }
  },

  enemyAttack: function(x, y, s, type){
    var bullet = this.knightBullets.getFirstDead();
    if(!bullet){return;}
    bullet.anchor.setTo(0.5, 0.5);
    bullet.scale.setTo(0.25, 0.25);
    bullet.reset(x, y);
    bullet.checkWorldBounds = true;
    bullet.outOfBoundsKill = true;
    bullet.body.velocity.x = 0;
    bullet.body.velocity.y = s;
    bullet.animations.play('backwalk');
  },

  bulletMove: function(){
    var playerX = this.player.x;
    var playerY = this.player.y;
    if(this.player.alive){
      this.knightBullets.forEachAlive(function(child){
        var dirX = (playerX - child.x);
        var dirY = (playerY - child.y);
        var len = Math.sqrt(dirY*dirY + dirX*dirX);
        child.body.velocity.x = 100 * dirX/len;
        child.body.velocity.y = 100 * dirY/len;
        if(child.body.velocity.y > 0){
          child.animations.play('frontwalk');
        }
        else{
          child.animations.play('backwalk');
        }
      });
    }
    else{
      this.knightBullets.forEachAlive(function(child){
        child.body.velocity.x = 0;
        child.body.velocity.y = -100;
      });
    }
  },

  enemyHurt: function(knight, bullet){
    this.emitter.x = knight.x;
    this.emitter.y = knight.y;
    this.emitter.start(true, 300, null, 5);
    
    bullet.kill();
    if(this.mana < 100){
      if((this.mana + 5) >= 100 && !this.manaReady){
        this.manaSound.play();
        this.manaReady = true;
      }
      this.mana += 5;
    }
    else{
      this.mana = 100;
    }
    this.playerMana();
    if(this.knightHP >= 500){
      this.enemyDie();
    }
    else{
      this.knightHP++;
    }
  },

  enemyDie: function(){
    this.knight.kill();

    this.emitter.x = this.knight.x;
    this.emitter.y = this.knight.y;
    this.emitter.start(true, 1000, null, 30);

    game.global.score += 100;
    this.scoreLabel.text = 'score: ' + game.global.score;

    this.backgroundSound.fadeOut(1000);

    game.time.events.add(2500, function(){
      this.backgroundSound.stop();
      game.state.start('over');
    }, this);
  },

  //UI
  playerHealth: function(){
    this.healthBar.clear();
    this.healthBar.lineStyle(0);
    if(this.playerHP < 5){
      this.healthBar.beginFill(0xEF0D0D, 0.5);
    }
    else{
      this.healthBar.beginFill(0xEF0D0D, 1);
    }
    this.healthBar.drawRect(60, 5, (10-this.playerHP)*10, 20);
    this.healthBar.endFill();
  },

  playerMana: function(){
    this.manaBar.clear();
    this.manaBar.lineStyle(0);
    if(this.mana == 100){
      this.manaBar.beginFill(0x0981D0, 1);
    }
    else{
      this.manaBar.beginFill(0x0981D0, 0.5);
    }
    
    this.manaBar.drawRect(60, 45, this.mana, 20);
    this.manaBar.endFill();
  },

  Pause: function(){
    if(!game.paused){
      this.pauseLabel.visible = true;
      game.paused = true;
    }
    else{
      this.pauseLabel.visible = false;
      game.paused = false;
    }
  },

  volumeControl: function(){
    this.volumeBar.clear();
    this.volumeBar.lineStyle(0);
    this.volumeBar.beginFill(0xffffff, 0.5);
    this.volumeBar.drawRect(game.width-60, 5, this.backgroundSound.volume*50, 20);
    this.volumeBar.endFill();
  }
}