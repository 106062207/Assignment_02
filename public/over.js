var overState = {
  create: function(){
    game.add.image(0, 0, 'background');

    if(game.global.score > game.global.bestScore){
      game.global.bestPlayer = game.global.player;
      game.global.bestScore = game.global.score;
      game.global.bestPlayerRef.set({
        name: game.global.bestPlayer,
        score: game.global.bestScore
      });
    }

    var nameLabel = game.add.text(game.width/2, 100, 'Game Over !!', {font: '50px Arial', fill: '#ffffff'});
    nameLabel.anchor.setTo(0.5, 0.5);

    var scoreLabel = game.add.text(game.width/2, 160, 'Score: ' + game.global.score, {font: '50px Arial', fill: '#ffffff'});
    scoreLabel.anchor.setTo(0.5, 0.5);


    var startLabel = game.add.text(game.width/2, 300, 'Play Again', {font: '50px Arial', fill: '#ffffff'});
    startLabel.anchor.setTo(0.5, 0.5);
    startLabel.inputEnabled = true;
    startLabel.events.onInputOver.add(this.over, this);
    startLabel.events.onInputOut.add(this.out, this);
    startLabel.events.onInputUp.add(this.upStart, this);

    var reLabel = game.add.text(game.width/2, 400, 'Menu', {font: '40px Arial', fill: '#ffffff'});
    reLabel.anchor.setTo(0.5, 0.5);
    reLabel.inputEnabled = true;
    reLabel.events.onInputOver.add(this.over, this);
    reLabel.events.onInputOut.add(this.out, this);
    reLabel.events.onInputUp.add(this.upMenu, this);
  },

  over: function(item){
    item.fill = "#EF0D0D";
  },

  out: function(item){
    item.fill = "#ffffff";
  },

  upStart: function(item){
    if(game.global.level == 1){
      game.state.start('play01');
    }
    else if(game.global.level == 2){
      game.state.start('play02');
    }
    else if(game.global.level == 3){
      game.state.start('play03');
    }
  },

  upMenu: function(item){
    game.state.start('menu');
  }
}