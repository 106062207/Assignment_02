var bootState = {
  preload: function(){
    game.load.image('progressBar', 'assets/progressBar.png');
  },

  create: function(){
    game.renderer.renderSession.roundPixels = true;
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.state.start('load');
  }
}