var levelState = {
  create: function(){
    game.add.image(0, 0, 'background');

    var level01Label = game.add.text(game.width/2, 100, 'Level01', {font: '50px Arial', fill: '#ffffff'});
    level01Label.anchor.setTo(0.5, 0.5);
    level01Label.inputEnabled = true;
    level01Label.events.onInputOver.add(this.over, this);
    level01Label.events.onInputOut.add(this.out, this);
    level01Label.events.onInputUp.add(this.up01, this);

    var level02Label = game.add.text(game.width/2, 200, 'Level02', {font: '50px Arial', fill: '#ffffff'});
    level02Label.anchor.setTo(0.5, 0.5);
    level02Label.inputEnabled = true;
    level02Label.events.onInputOver.add(this.over, this);
    level02Label.events.onInputOut.add(this.out, this);
    level02Label.events.onInputUp.add(this.up02, this);

    var level03Label = game.add.text(game.width/2, 300, 'Level03', {font: '50px Arial', fill: '#ffffff'});
    level03Label.anchor.setTo(0.5, 0.5);
    level03Label.inputEnabled = true;
    level03Label.events.onInputOver.add(this.over, this);
    level03Label.events.onInputOut.add(this.out, this);
    level03Label.events.onInputUp.add(this.up03, this);
  },

  over: function(item){
    item.fill = "#EF0D0D";
  },

  out: function(item){
    item.fill = "#ffffff";
  },

  up01: function(item){
    game.state.start('play01');
    game.global.level = 1;
  },

  up02: function(item){
    game.state.start('play02');
    game.global.level = 2;
  },

  up03: function(item){
    game.state.start('play03');
    game.global.level = 3;
  }
}