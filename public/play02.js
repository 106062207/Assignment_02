var play02State = {
  create: function(){
    //map
    this.map = game.add.tileSprite(0, 0, 450, 550, 'background');
    this.backgroundSound = game.add.audio('BGM01');
    this.backgroundSound.loop = true;
    this.backgroundSound.volume = 0.3;
    this.backgroundSound.play();

    //player
    this.player = game.add.sprite(game.width/2, game.height/2, 'player');
    game.physics.arcade.enable(this.player);
    this.player.anchor.setTo(0.5, 1);
    this.player.animations.add('frontwalk', [0, 2], 8, true);
    this.player.animations.add('leftwalk', [3, 5], 8, true);
    this.player.animations.add('rightwalk', [6, 8], 8, true);
    this.player.animations.add('backwalk', [9, 11], 8, true);
    this.player.checkWorldBounds = true;
    this.playerHP = 0;
    this.playerDieSound = game.add.audio('playerDie');
    this.playerUltSound = game.add.audio('ult');
    this.playerBulletType = 'normal';
    this.bulletEffectCount = 0;

    //playerMove
    this.dir = 0;

    //input
    this.cursor = game.input.keyboard.createCursorKeys();
    this.zKey = game.input.keyboard.addKey(Phaser.Keyboard.Z);
    this.xKey = game.input.keyboard.addKey(Phaser.Keyboard.X);
    this.escKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
    this.shiftKey = game.input.keyboard.addKey(Phaser.Keyboard.SHIFT);
    this.wKey = game.input.keyboard.addKey(Phaser.Keyboard.W);
    this.sKey = game.input.keyboard.addKey(Phaser.Keyboard.S);

    //bullet
    this.bullets = game.add.group();
    this.bullets.enableBody = true;
    this.bullets.createMultiple(20, 'bullet');
    this.bulletCount = 0;
    this.bulletAngle = [Math.PI*(1/8), Math.PI*(1/4), Math.PI*(3/8), Math.PI*(1/2), Math.PI*(5/8), Math.PI*(3/4), Math.PI*(7/8)];

    //enemy: golem
    this.golems = game.add.group();
    this.golems.enableBody = true;
    this.golems.createMultiple(10, 'golem01');
    this.golems.callAll('animations.add', 'animations', 'frontwalk', [0, 2], 8, true);
    this.golems.callAll('animations.add', 'animations', 'leftwalk', [3, 5], 8, true);
    this.golems.callAll('animations.add', 'animations', 'rightwalk', [6, 8], 8, true);
    this.golems.callAll('animations.add', 'animations', 'backwalk', [9, 11], 8, true);
    this.golemCount = 0;
    this.golemBullets = game.add.group();
    this.golemBullets.enableBody = true;
    this.golemBullets.createMultiple(50, 'golemBullet');
    this.bulletCountGolem = [];
    this.bulletCountGolem.length = 10;
    this.golemHP = [];
    this.golemHP.length = 10;
    for(var i = 0; i < 10; i++){
      this.golemHP[i] = 0;
    }
    this.golemDieSound = game.add.audio('golemDie');
    this.golemDieSound.volume = 0.5;

    //enemy: dragon
    this.dragons = game.add.group();
    this.dragons.enableBody = true;
    this.dragons.createMultiple(10, 'dragon01');
    this.dragons.callAll('animations.add', 'animations', 'frontwalk', [0, 2], 8, true);
    this.dragons.callAll('animations.add', 'animations', 'leftwalk', [3, 5], 8, true);
    this.dragons.callAll('animations.add', 'animations', 'rightwalk', [6, 8], 8, true);
    this.dragons.callAll('animations.add', 'animations', 'backwalk', [9, 11], 8, true);
    this.dragonCount = 0;
    this.dragonBullets = game.add.group();
    this.dragonBullets.enableBody = true;
    this.dragonBullets.createMultiple(50, 'dragonBullet');
    this.bulletCountDragon = [];
    this.bulletCountDragon.length = 10;
    this.dragonHP = [];
    this.dragonHP.length = 10;
    for(var i = 0; i < 10; i++){
      this.dragonHP[i] = 0;
    }

    //item
    this.items = game.add.group();
    this.items.enableBody = true;
    this.items.createMultiple(5, 'item');
    this.itemCount = 600;

    //playerHealth
    this.healthLabel = game.add.text(5, 5, 'Health', {font:'18px Arial', fill:'#ffffff'});
    this.healthBar = game.add.graphics(0, 0);
    this.playerHealth();

    //mana
    this.manaLabel = game.add.text(5, 45, 'Mana', {font:'18px Arial', fill:'#ffffff'});
    this.manaBar = game.add.graphics(0, 0);
    this.mana = 5;
    this.playerMana();
    this.manaSound = game.add.audio('mana');
    this.manaReady = false;

    //score
    this.scoreLabel = game.add.text(5, 85, 'score: 0', {font:'18px Arial', fill:'#ffffff'});
    game.global.score = 0;
 
    //volume
    this.volumeLabel = game.add.text(game.width-120, 5, 'BGM', {font:'18px Arial', fill:'#ffffff'});
    this.volumeBar = game.add.graphics(0, 0);
    this.volumeControl();
    this.wKey.onDown.add(function(){
      if((this.backgroundSound.volume+0.1) >= 1){
        this.backgroundSound.volume = 1;
      }
      else{
        this.backgroundSound.volume += 0.1;
      }
    }, this);
    this.sKey.onDown.add(function(){
      if((this.backgroundSound.volume-0.1) <= 0){
        this.backgroundSound.volume = 0;
      }
      else{
        this.backgroundSound.volume -= 0.1;
      }
    }, this);


    //pause
    this.escKey.onDown.add(this.Pause, this);
    this.pauseLabel = game.add.text(game.width-40, 35, 'Pause', {font:'50px, Arial', fill:'#ffffff'});
    this.pauseLabel.visible = false;

    //particle
    this.emitter = game.add.emitter(0, 0, 15);
    this.emitter.makeParticles('pixel');
    this.emitter.setYSpeed(-150, 150);
    this.emitter.setXSpeed(-150, 150);
    this.emitter.setScale(1.5, 0, 1.5, 0, 800);
    this.emitter.gravity = 0;
  },

  update: function(){
    if(this.player.alive){
      this.map.tilePosition.y -= 2;
      game.physics.arcade.overlap(this.player, this.golemBullets, function(player, bullet){
        this.playerHurt(bullet, 'golem');
      }, null, this);
      game.physics.arcade.overlap(this.golems, this.bullets, function(child, bullet){
        this.golemHurt(child, bullet);
      }, null, this);
  
      game.physics.arcade.overlap(this.player, this.dragonBullets, function(player, bullet){
        this.playerHurt(bullet, 'dragon');
      }, null, this);
      game.physics.arcade.overlap(this.dragons, this.bullets, function(child, bullet){
        this.dragonHurt(child, bullet);
      }, null, this);

      game.physics.arcade.overlap(this.player, this.items, function(player, item){
        this.playerBulletType = 'armor-piercing';
        item.kill();
        this.bulletCount = 0;
      }, null, this);
    }

    this.itemGen();

    this.playerMove();

    this.enemyGen();

    this.enemyMove();

    if(this.playerBulletType == 'armor-piercing'){
      if(this.bulletEffectCount == 600){
        this.playerBulletType = 'normal';
        this.bulletEffectCount = 0;
        this.bulletCount = 0;
      }
      else{
        this.bulletEffectCount++;
      }
    }

    if(!this.player.inWorld){
      this.playerDie();
    }

    this.volumeControl();
  },

  //player
  playerMove: function(){
    var speed;
    if(this.shiftKey.isDown){
      speed = 50;
    }
    else{
      speed = 200;
    }
    if(this.cursor.left.isDown){
      this.dir = 1;
      this.player.body.velocity.x = speed * (-1);
      this.player.animations.play('leftwalk');
    }
    else if(this.cursor.right.isDown){
      this.dir = 2;
      this.player.body.velocity.x = speed;
      this.player.animations.play('rightwalk');
    }
    else{
      this.player.body.velocity.x = 0;
      if(this.dir == 1){
        this.player.frame = 3;
      }
      else if(this.dir == 2){
        this.player.frame = 6;
      }
    }

    if(this.cursor.up.isDown){
      this.dir = 4;
      this.player.body.velocity.y = speed * (-1);
      this.player.animations.play('backwalk');
    }
    else if(this.cursor.down.isDown){
      this.dir = 0;
      this.player.body.velocity.y = speed;
      this.player.animations.play('frontwalk');
    }
    else{
      this.player.body.velocity.y = 0;
      if(this.dir == 0){
        this.player.frame = 0;
      }
      else if(this.dir == 4){
        this.player.frame = 9;
      }
    }

    if(this.player.x <= 16){
      this.player.x = 16;
    }
    else if(this.player.x >= (game.width-16)){
      this.player.x = (game.width-16);
    }

    if(this.player.y <= 32){
      this.player.y = 32;
    }
    else if(this.player.y >= (game.height-32)){
      this.player.y = (game.height-32);
    }

    if(this.zKey.isDown){
      this.attack(this.player.x, this.player.y, 200);
    }

    if(this.xKey.isDown && this.mana == 100){
      this.playerUlt();
      this.manaReady = false;
      this.mana = 0;
    }
  },

  attack: function(x, y, s){
    if(this.playerBulletType == 'normal'){
      if(this.bulletCount == 10){
        var bullet = this.bullets.getFirstDead();
        if(!bullet){return;}
        bullet.anchor.setTo(0.5, 0.5);
        bullet.scale.setTo(0.25, 0.25);
        bullet.reset(x, y);
        bullet.checkWorldBounds = true;
        bullet.outOfBoundsKill = true;
        bullet.body.velocity.x = 0;
        bullet.body.velocity.y = s;

        this.bulletCount = 0;
      }
      else{
        this.bulletCount++;
      }
    }
    else if(this.playerBulletType == 'armor-piercing'){
      if(this.bulletCount == 120){
        var bullet = this.bullets.getFirstDead();
        if(!bullet){return;}
        bullet.anchor.setTo(0.5, 0.5);
        bullet.scale.setTo(0.5, 0.5);
        bullet.reset(x, y);
        bullet.checkWorldBounds = true;
        bullet.outOfBoundsKill = true;
        bullet.body.velocity.x = 0;
        bullet.body.velocity.y = s * 1.5;
        
        this.bulletCount = 0;
      }
      else{
        this.bulletCount++;
      }
    }
  },

  playerUlt: function(){
    this.playerUltSound.play();
    this.golems.forEachAlive(function(child){
      this.enemyDie(child);
    }, this);
    this.golemBullets.forEachAlive(function(child){
      child.kill();
    })
    this.dragons.forEachAlive(function(child){
      this.dragonHP[this.dragons.getIndex(child)] += 10;
    }, this);
    this.dragonBullets.forEachAlive(function(child){
      child.kill();
    })
  },

  playerHurt: function(bullet, type){
    //console.log(this.playerHP);
    this.emitter.x = this.player.x;
    this.emitter.y = this.player.y;
    this.emitter.start(true, 300, null, 5);
    
    bullet.kill();
    this.playerHealth();
    if(this.playerHP >= 10){
      this.playerHP = 10;
      this.playerDie();
    }
    else{
      if(type == 'golem'){
        this.playerHP += 0.5;
      }
      else if(type == 'dragon'){
        this.playerHP += 2;
      }
    }
  },

  playerDie: function(){
    this.playerDieSound.play();
    this.player.kill();

    this.emitter.x = this.player.x;
    this.emitter.y = this.player.y;
    this.emitter.start(true, 800, null, 15);

    game.camera.shake(0.02, 300);

    this.backgroundSound.fadeOut(1000);

    game.time.events.add(2500, function(){
      this.backgroundSound.stop();
      game.state.start('over');
    }, this);
  },

  //enemy
  enemyGen: function(){
    if(this.golemCount == 200){
      var index = game.rnd.pick([1, 2, 3]);
      for(var i = 0; i < index; i++){
        var golem = this.golems.getFirstDead();
        if(!golem){return;}
        this.bulletCountGolem[this.golems.getIndex(golem)] = 40;
        var position = [{x: game.width/3, y: game.height-42}, 
                        {x: game.width * (2/3), y: game.height-42},
                        {x: game.width/2, y: game.height-42}];
        var newPos = game.rnd.pick(position);
        golem.anchor.setTo(0.5, 0);
        golem.reset(newPos.x, newPos.y);
        golem.body.velocity.x = 100 * game.rnd.pick([-1, 0, 1]);
        golem.body.velocity.y = 100 * game.rnd.pick([-1, 0, 1]);
      }

      this.golemCount = 0;
    }
    else{
      this.golemCount++;
    }

    if(this.dragonCount == 700){
      var dragon = this.dragons.getFirstDead();
      if(!dragon){return;}
      this.bulletCountDragon[this.dragons.getIndex(dragon)] = 0;
      var position = [{x: game.width/4, y: game.height-42}, 
                      {x: game.width/2, y: game.height-42}, 
                      {x: game.width*(3/4), y: game.height-42}];
      var newPos = game.rnd.pick(position);
      dragon.anchor.setTo(0.5, 0);
      dragon.reset(newPos.x, newPos.y);
      dragon.body.velocity.x = 80 * game.rnd.pick([-1, 0, 1]);
      dragon.body.velocity.y = 80 * game.rnd.pick([-1, 0, 1]);

      this.dragonCount = 0;
    }
    else{
      this.dragonCount++;
    }
  },

  enemyMove: function(){
    this.golems.forEachAlive(function(child){
      if(this.bulletCountGolem[this.golems.getIndex(child)] == 40){
        this.enemyAttack(child.x, child.y, -150, 'golem');
        this.bulletCountGolem[this.golems.getIndex(child)] = 0;
      }
      else{
        this.bulletCountGolem[this.golems.getIndex(child)]++;
      }

      if(child.body.velocity.y < 0){
        child.animations.play('backwalk');
        if(child.y <= (game.height/2)){
          child.body.velocity.y *= -1;
        }

        if(child.body.velocity.x < 0 && child.x <= 21){
          child.body.velocity.x *= -1;
        }
        if(child.body.velocity.x > 0 && child.x >= (game.width-21)){
          child.body.velocity.x *= -1;
        }
      }
      else if(child.body.velocity.y > 0){
        child.animations.play('frontwalk');
        if(child.y >= (game.height - 21)){
          child.body.velocity.y *= -1;
        }

        if(child.body.velocity.x < 0 && child.x <= 21){
          child.body.velocity.x *= -1;
        }
        if(child.body.velocity.x > 0 && child.x >= (game.width-21)){
          child.body.velocity.x *= -1;
        }
      }
      else{
        if(child.body.velocity.x < 0){
          child.animations.play('leftwalk');
          if(child.x <= 21){
            child.body.velocity.x *= -1;
          }
        }
        else if(child.body.velocity.x > 0){
          child.animations.play('rightwalk');
          if(child.x >= (game.width-21)){
            child.body.velocity.x *= -1;
          }
        }
      }
    }, this);

    this.dragons.forEachAlive(function(child){
      if(this.bulletCountDragon[this.dragons.getIndex(child)] == 300){
        this.enemyAttack(child.x, child.y, -100, 'dragon');
        this.bulletCountDragon[this.dragons.getIndex(child)] = 0;
      }
      else{
        this.bulletCountDragon[this.dragons.getIndex(child)]++;
      }

      if(child.body.velocity.y < 0){
        child.animations.play('backwalk');
        if(child.y <= (game.height/2)){
          child.body.velocity.y *= -1;
        }

        if(child.body.velocity.x < 0 && child.x <= 40){
          child.body.velocity.x *= -1;
        }
        if(child.body.velocity.x > 0 && child.x >= (game.width-40)){
          child.body.velocity.x *= -1;
        }
      }
      else if(child.body.velocity.y > 0){
        child.animations.play('frontwalk');
        if(child.y >= (game.height - 32)){
          child.body.velocity.y *= -1;
        }

        if(child.body.velocity.x < 0 && child.x <= 40){
          child.body.velocity.x *= -1;
        }
        if(child.body.velocity.x > 0 && child.x >= (game.width-40)){
          child.body.velocity.x *= -1;
        }
      }
      else{
        if(child.body.velocity.x < 0){
          child.animations.play('leftwalk');
          if(child.x <= 40){
            child.body.velocity.x *= -1;
          }
        }
        else if(child.body.velocity.x > 0){
          child.animations.play('rightwalk');
          if(child.x >= (game.width-40)){
            child.body.velocity.x *= -1;
          }
        }
      }
    }, this);
  },

  enemyAttack: function(x, y, s, type){
    if(type == 'golem'){
      var bullet = this.golemBullets.getFirstDead();
      if(!bullet){return;}
      bullet.anchor.setTo(0.5, 0.5);
      bullet.scale.setTo(0.25, 0.25);
      bullet.reset(x, y);
      bullet.checkWorldBounds = true;
      bullet.outOfBoundsKill = true;
      var angle = game.rnd.pick([2, 4]);
      bullet.body.velocity.x = s * Math.cos(this.bulletAngle[angle]);
      bullet.body.velocity.y = s * Math.sin(this.bulletAngle[angle]);
    }
    else if(type == 'dragon'){
      for(var i = 0; i < 7; i++){
        var bullet = this.dragonBullets.getFirstDead();
        if(!bullet){return;}
        bullet.anchor.setTo(0.5, 0.5);
        bullet.scale.setTo(0.25, 0.25);
        bullet.reset(x, y);
        bullet.checkWorldBounds = true;
        bullet.outOfBoundsKill = true;
        
        bullet.body.velocity.x = s * Math.cos(this.bulletAngle[i]);
        bullet.body.velocity.y = s * Math.sin(this.bulletAngle[i]);
      }
    }
  },

  golemHurt: function(child, bullet){
    //console.log(this.golemHP[this.golems.getIndex(child)]);
    this.emitter.x = child.x;
    this.emitter.y = child.y;
    this.emitter.start(true, 300, null, 5);
    
    bullet.kill();
    if(this.mana < 100){
      if((this.mana + 5) >= 100 && !this.manaReady){
        this.manaSound.play();
        this.manaReady = true;
      }
      this.mana += 5;
    }
    this.playerMana();
    if(this.golemHP[this.golems.getIndex(child)] >= 2){
      this.golemHP[this.golems.getIndex(child)] = 0;
      this.enemyDie(child, 'golem');
    }
    else{
      if(this.playerBulletType == 'normal'){
        this.golemHP[this.golems.getIndex(child)]++;
      }
      else if(this.playerBulletType == 'armor-piercing'){
        this.golemHP[this.golems.getIndex(child)] += 2;
      }
    }
  },

  dragonHurt: function(child, bullet){
    this.emitter.x = child.x;
    this.emitter.y = child.y;
    this.emitter.start(true, 300, null, 5);

    bullet.kill();
    if(this.mana < 100){
      if((this.mana + 5) >= 100 && !this.manaReady){
        this.manaSound.play();
        this.manaReady = true;
      }
      this.mana += 5;
    }
    this.playerMana();
    if(this.dragonHP[this.dragons.getIndex(child)] >= 20){
      this.dragonHP[this.dragons.getIndex(child)] = 0;
      this.enemyDie(child, 'dragon');
    }
    else{
      if(this.playerBulletType == 'normal'){
        this.dragonHP[this.dragons.getIndex(child)]++;
      }
      else if(this.playerBulletType == 'armor-piercing'){
        this.dragonHP[this.dragons.getIndex(child)] += 5;
      }
    }
  },

  enemyDie: function(child, type){
    if(type == 'golem'){
      this.golemDieSound.play();
      game.global.score += 1;
    }
    else if(type == 'dragon'){
      game.global.score += 5;
    }

    child.kill();

    this.emitter.x = child.x;
    this.emitter.y = child.y;
    this.emitter.start(true, 600, null, 15);

    this.scoreLabel.text = 'score: ' + game.global.score;
  },

  itemGen: function(){
    if(this.itemCount == 1200){
      var item = this.items.getFirstDead();
      if(!item){return;}
      item.anchor.setTo(0.5, 0.5);
      item.scale.setTo(0.5, 0.5);
      var itemX = game.rnd.pick([1, 1.5, 2, 2.5, 3, 3.5, 4]) * 100;
      var itemY = game.rnd.pick([1, 1.5, 2, 2.5, 3]) * 100;
      item.reset(itemX, itemY);
      game.add.tween(item.scale).to({x: 0.5, y: 0.5}, 300).start();

      this.itemCount = 0;
    }
    else{
      this.itemCount++;
    }
  },

  //UI
  playerHealth: function(){
    this.healthBar.clear();
    this.healthBar.lineStyle(0);
    if(this.playerHP < 5){
      this.healthBar.beginFill(0xEF0D0D, 0.5);
    }
    else{
      this.healthBar.beginFill(0xEF0D0D, 1);
    }
    this.healthBar.drawRect(60, 5, (10-this.playerHP)*10, 20);
    this.healthBar.endFill();
  },

  playerMana: function(){
    this.manaBar.clear();
    this.manaBar.lineStyle(0);
    if(this.mana == 100){
      this.manaBar.beginFill(0x0981D0, 1);
    }
    else{
      this.manaBar.beginFill(0x0981D0, 0.5);
    }
    
    this.manaBar.drawRect(60, 45, this.mana, 20);
    this.manaBar.endFill();
  },

  Pause: function(){
    if(!game.paused){
      this.pauseLabel.visible = true;
      game.paused = true;
    }
    else{
      this.pauseLabel.visible = false;
      game.paused = false;
    }
  },

  volumeControl: function(){
    this.volumeBar.clear();
    this.volumeBar.lineStyle(0);
    this.volumeBar.beginFill(0xffffff, 0.5);
    this.volumeBar.drawRect(game.width-60, 5, this.backgroundSound.volume*50, 20);
    this.volumeBar.endFill();
  }
}