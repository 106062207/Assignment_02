var menuState = {
  create: function(){
    game.add.image(0, 0, 'background');
    this.backgroundSound = game.add.audio('menu');
    this.backgroundSound.loop = true;
    this.backgroundSound.volume = 0.5;
    this.backgroundSound.play();

    var nameLabel = game.add.text(game.width/2, 100, 'Assignment02', {font: '50px Arial', fill: '#ffffff'});
    nameLabel.anchor.setTo(0.5, 0.5);
    
    var startLabel = game.add.text(game.width/2, 300, 'Start', {font: '50px Arial', fill: '#ffffff'});
    startLabel.anchor.setTo(0.5, 0.5);
    startLabel.inputEnabled = true;
    startLabel.events.onInputOver.add(this.over, this);
    startLabel.events.onInputOut.add(this.out, this);
    startLabel.events.onInputUp.add(this.up, this);

    var scoreLabel = game.add.text(game.width/2, 450, 'Score: ' + game.global.score, {font: '18px Arial', fill: '#ffffff'});
    scoreLabel.anchor.setTo(0.5, 0.5);
    var bestLabel = game.add.text(game.width/2, 500, 'BestPlayer: ' + game.global.bestPlayer + ' ' + game.global.bestScore, {font: '18px Arial', fill: '#ffffff'});
    bestLabel.anchor.setTo(0.5, 0.5);
  },

  over: function(item){
    item.fill = "#EF0D0D";
  },

  out: function(item){
    item.fill = "#ffffff";
  },

  up: function(item){
    this.backgroundSound.fadeOut(800);
    game.time.events.add(1000, function(){
      game.state.start('level');
    });
  }
}