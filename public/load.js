var loadState = {
  preload: function(){
    game.global.bestPlayerRef.on('value', function(snapshot){
      game.global.bestPlayer = snapshot.val()['name'];
      game.global.bestScore = snapshot.val()['score'];
    });

    game.renderer.renderSession.roundPixels = true;

    game.physics.startSystem(Phaser.Physics.ARCADE);

    game.load.image('background', 'assets/background.png');
    game.load.audio('menu', ['assets/menu.ogg', 'assets/menu.mp3']);
    game.load.audio('BGM01', ['assets/BGM01.ogg', 'assets/BGM01.mp3']);
    game.load.audio('BGM02', ['assets/BGM02.ogg', 'assets/BGM02.mp3']);

    game.load.spritesheet('player', 'assets/player.png', 32, 32);
    game.load.image('bullet', 'assets/bullet.png');
    game.load.audio('playerDie', ['assets/playerDie.ogg', 'assets/playerDie.mp3']);
    game.load.audio('ult', ['assets/ult.ogg', 'assets/ult.mp3']);

    game.load.spritesheet('helper', 'assets/fairy01.png', 50, 42);

    game.load.spritesheet('golem01', 'assets/golem01.png', 42, 42);
    game.load.image('golemBullet', 'assets/golemBullet.png');
    game.load.audio('golemDie', ['assets/golemDie.ogg', 'assets/golemDie.mp3']);
    
    game.load.spritesheet('dragon01', 'assets/dragon01.png', 80, 64);
    game.load.image('dragonBullet', 'assets/dragonBullet.png');

    game.load.spritesheet('knight01', 'assets/knight01.png', 80, 80);
    game.load.spritesheet('knightBullet', 'assets/sword.png', 60, 120);

    game.load.image('pixel', 'assets/pixel.png');

    game.load.audio('mana', ['assets/mana.ogg', 'assets/mana.mp3']);

    game.load.image('item', 'assets/item.png');
  },

  create: function(){
    game.global.player = prompt("Please enter your name", "name");
    game.state.start('menu');
  }
}