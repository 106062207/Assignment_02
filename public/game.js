var game = new Phaser.Game(450, 550, Phaser.AUTO);

game.global = {score: 0,
               level: 0, 
               player: '',
               bestPlayerRef: firebase.database().ref('bestPlayer'),
               bestPlayer: '',
               bestScore: 0
};

game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('level', levelState);
game.state.add('play01', play01State);
game.state.add('play02', play02State);
game.state.add('play03', play03State);
game.state.add('over', overState);
game.state.start('load');