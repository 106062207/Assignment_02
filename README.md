# Assignment_02

##Topic
*Project Name: Assignment_02_106062207

##Basic Components
  1. complete game process:
    -start menu = => game view => game over && quit or play again
  2. Basic Rules:
    -Follow the basic rules of Raiden
      • player : can move, shoot and it’s life will decrease when touch enemy’s bullets
      • Enemy : system could generate enemy and each enemy can move and attack
      • Map : background will move through the game
  3. Jucify mechenisms:
    –Level : having different levels to play
    –Skill : player can use ultimate skills to attack enemy
  4. Animations:
    -Add animation to player
  5. Particle Systems:
    –Add particle systems to player’s and enemy’s bullets
  6. Sound effects:
    –At least two kinds of sound effect
    –Can change volume
  7. UI:
    –Player health, Ultimate skill number or power, Score, Volume control and Game pause
  8. Leaderboard:
    –Store player's name and score in firebase real time database, and design a way to show a leaderboard to your game
  9. Appearence

##Bonus Components
  1. Enhanced Items:
    Player gets the item then it can become more powerful.
    • Bullet automatic aiming
    • Unique bullet
    • Little helper
  2. Boss:
    • Unique movement and attack mode

## Website Detail Description

#作品網址: https://106062207.gitlab.io/Assignment_02

#Components Description:
  1. complete game process:
    start menu: press enter to process forward/showing score after game over/showing best player's name and score
    game view: 3 levels to select
    game over && quit or play again: showing score/press play again to play again/press menu to get back to menu/close the browser to quit the game
  2. Basic Rules:
    • player: press up, down, left, right arrow to move(press shift to slow down)/press z to shoot/it’s life will decrease when touch enemy’s bullets(**Note: the first time the player touches bullet would not decrease it's life for warning**)
    • enemy: enemy would be generated at som specific random place/each enemy has different attck mode
    • map: background will move through the game
  3. Jucify mechenisms:
    • level: level01: 1 kind of enemy/level02: 2 kind of enemies/level03: only 1 boss
    • skill: press x to eliminate all bullets on the field and do damage to each enemy respectively
  4. Animations:
    play different animations when moving at different directions
  5. Particle Systems:
    when bullets hit the target(enemy/player)/player or the boss in level3 dies, particle system would triggered
  6. Sound effects:
    BGM: menu/each level
    effect sound: golems in level1 and level2 dies/player dies/mana is ready for ult
    Can change volume: press w/d to increase/decrease volume
  7. UI:
    player health: when health is low, the health bar would become more obvious
    mana(for ult): when mana is ready, the mana bar would become more obvious
    score: update each time you get the score
    volume control: press w/d to increase/decrease volume/volume would show as a bar
    game pause: press esc to puase/showing pause label while the game is paused
  8. Leaderboard:
    the best player name and score would be showed on menu, and if the current player gets higher score then the best player, the best player would update
  9. Enhanced Items:
    player can get the woodenbox which appear after every period of time to have special power
    1. Bullet automatic aiming: in level1, player would get the auto-aiming bullet
    2. Unique bullet: in level2, player would get the armor-piercing bullet which has faster speed and do more damage while having lower attack speed
    3. Little Helper: in level3, player would get a helper which only exist for a period of time and would enhance the firepower
  10. Boss:
    in level03, there is only 1 enemy, which has auto-aiming bullet and would speed up when health is low
  11. Other functions:
    1. while pressing shift, the player's move speed would slowdown in order to have more accurate movement
    2. BGM would fadeout while changing scene in order to improve player experience